package kris.rxservices.service;

import android.util.Log;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subjects.BehaviorSubject;

/**
 * Created by admin on 13.03.2017.
 */

public class ViewConnectionObserver<T> {
    private static final String TAG = ViewConnectionObserver.class.getSimpleName();

    BehaviorSubject<T> viewSubject = BehaviorSubject.create();

    Observer<T> observer = new viewObserver();
    private Subscription subscription;

    /*package*/void connect(Observable<T> view){
        if (subscription!=null && !subscription.isUnsubscribed()){
            subscription.unsubscribe();
            viewSubject.onNext(null);
        }
        subscription = view.subscribe(observer);
    }

    public Observable<T> getView(){
        return viewSubject.filter(t -> t!=null).first();
    }

    private class viewObserver implements Observer<T> {
        @Override
        public void onCompleted() {
            viewSubject.onNext(null);
        }

        @Override
        public void onError(Throwable e) {
            Log.wtf(TAG, "Error in ViewConnectionObserver");
            e.printStackTrace();
            viewSubject.onNext(null);
        }

        @Override
        public void onNext(T t) {
            viewSubject.onNext(t);

        }
    }
}
