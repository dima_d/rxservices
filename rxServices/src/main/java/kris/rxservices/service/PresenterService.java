package kris.rxservices.service;

import android.content.Context;

import rx.Observable;

/**
 * Created by admin on 13.03.2017.
 */

public class PresenterService<V> extends BoundService {
    protected ViewConnectionObserver<V> getViewConnectionObserver() {
        return view;
    }

    protected Observable<V> getView() {
        return view.getView();
    }

    private ViewConnectionObserver<V> view = new ViewConnectionObserver<V>();

    public static <T extends PresenterService<V>, V> ServiceConnector<T> connector(Context context, Class<T> service, Observable<V> view ){
        return new PresenterServiceConnector<T, V>(context, service,view);
    }

    public static <T extends PresenterService<V>, V>  ServiceConnector<T> connector(Context context, Class<T> service, Observable.Transformer<T, T> lifecycle, V view){
        return new PresenterServiceConnector<T, V>(context, service, lifecycle, view);
    }

}
