package kris.rxservices.service;

import android.content.Context;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by admin on 13.03.2017.
 */

public class PresenterServiceConnector<T extends PresenterService<V>, V> extends ServiceConnector<T> {
    public PresenterServiceConnector(Context context, Class<T> tClass, Observable<V> view) {
        super(context, tClass);
        serviceSubject.first().subscribe(service->{
            service.getViewConnectionObserver().connect(view);
        });
    }

    public PresenterServiceConnector(Context context, Class<T> tClass, Observable.Transformer<T, T> lifeCycle, V view) {
        super(context, tClass, lifeCycle);
        Observable<V> compose = BehaviorSubject.create(view).compose((Observable.Transformer<? super V, ? extends V>) lifeCycle);

        serviceSubject.first().subscribe(service->{
            service.getViewConnectionObserver().connect(compose);
        });
    }
}
