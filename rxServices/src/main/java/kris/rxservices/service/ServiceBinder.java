package kris.rxservices.service;

import android.os.Binder;

import kris.rxservices.service.BoundService;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
public class ServiceBinder extends Binder {

    private BoundService service;

    public ServiceBinder(BoundService service) {
        this.service = service;
    }

    public BoundService getService() {
        return service;
    }
}
